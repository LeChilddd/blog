<?php

class database{
    public $host = "localhost";
    public $usernamebdd = "admin";
    public $passwordbdd = "admin";
    public $dbName = "blog";

    public function connect(){

        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $pdo = new PDO($dsn, $this->usernamebdd, $this->passwordbdd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
    
}