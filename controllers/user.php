<?php
class user extends database{

    public $name;
    public $username;
    public $email;
    public $password;
    public $userType;

    public function __construct($name, $username, $email, $password,$userType)
    {
        $this->name = $name;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->userType = $userType;
    }

    public function addUserInBdd(){
        $sql = 'INSERT INTO `user`(username, email, password, user_type) VALUES (? ? ? ?)';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$this->name, $this->username, $this->email, $this->password, $this->userType]);
    }

}