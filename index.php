<?php

// Load the autoload file to make all the classes in the vendor
// directory available to our app.
require_once __DIR__.'/vendor/autoload.php';

// Instantiate the Silex application.
$app = new Silex\Application();

// Register the Twig service provider and let it know where to look for templates.
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
  ));

// Enable debuggin mode for developer friendly error messages.
$app['debug'] = true;

// Create route for the root of the site and return 'hello world'.
$app->get('/', function () use ($app) {
    return $app['twig']->render('home.twig', ['person' => [
        'name' => 'jean',
        'age' => '18',
    ]]);
})->bind('home');

$app->get('/log', function () use ($app) {
  return $app['twig']->render('log.twig');
})->bind('connect');



$app->run();
